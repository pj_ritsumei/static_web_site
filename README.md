# PJのWebページ管理
## ディレクトリ構成

root/  
┠ article //記事を格納するファイル   
　┗ 記事のjsonファイル形式   
┠ template_news_html  
┠ make_new_html.rb  
┠ original.json  
┗ README.md //readme   

   
## news の生成
以下のコマンドを実行するとnews.htmlが作成されます.  

	$ rm news.html // 任意
	$ ruby make_new_html.rb

## 記事のJSONファイル
original.json にテンプレートデータが格納されています.  
作成したデータは article以下に保存してください.

	{
	  "org" : 団体名,
	  "create-date-created" : 日付,
	  "title" : 記事のタイトル,
	  "body" : [記事行1,
	            記事行2]
	}
		
団体 > Ri-one, RiPPro, RiG++, PJ団体
