require 'json'

def load_json(file_name)
  JSON.load(File.open(file_name))
end

def get_json_data
  directory = Dir.glob("./article/20*")  

  articles = []
  for article in directory
    articles.push(load_json(article))
  end
  articles.reverse
end

def get_article(json)
  s = json["body"].join("<br>")
  "<table id='tabledata' width='100%'' class='span3 picture-item' data-groups='["+json["org"]+"]' data-date-created='"+json["create-date-created"]+"""''>
      <tr>
        <td>
          <div class='panel panel-info'>
            <div class='panel-heading'>
              <h3 class='panel-title'>"""+json["title"]+"""</h3>
            </div>
            <div class='panel-body'>"""+s+"""
            </div>
          </div>
        </td>
      </tr>
    </table>
    """
end

def get_news_html(file_name)
  news_file = File.open('news.html','w')
  begin
    File.open(file_name) do |file|
      file.each_line do |labmen|
        if labmen.match("yield") 
          articles = get_json_data

          for article in articles
            news_file.puts get_article(article)
          end

        else 
          news_file.puts labmen
        end
      end
    end
  rescue SystemCallError => e
    puts %Q(class=[#{e.class}] message=[#{e.message}])
  rescue IOError => e
    puts %Q(class=[#{e.class}] message=[#{e.message}])
  end
  news_file.close
end 
get_news_html('template_news.html')